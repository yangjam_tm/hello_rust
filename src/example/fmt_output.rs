/// # 格式化输出
/// 打印操作由 `std::fmt` 里面定义的一系列宏来处理:
/// - `format!`: 将格式化文本写入到字符串
/// - `print!`: 类似 `format!`, 但将文本输出到控制台(io:stdout)
/// - `println!`: 类似 `print!`, 但会在输出内容的结尾添加一个换行符
/// - `eprint!`: 类似 `print!`, 但将文本输出到标准错误(io::stderr)
/// - `eprintln!`: 类似 `eprint!`, 但会在输出内容的结尾添加一个换行符

fn get_mod_name() -> String {
    std::module_path!().to_string()
}

#[allow(dead_code)] // 忽略未使用的代码警告
pub fn test() {
    println!("Running example: {}", get_mod_name());
    
    // `{}`` 作为占位符，输出时会被后面的参数替换
    println!("{} days", 31);

    // 在 `{}` 中使用位置参数
    println!("{0}, this is {1}. {1}, this is {0}", "Alice", "Bob");

    // 使用命名参数
    println!(
        "{subject} {verb} {object}",
        object = "the lazy dog",
        subject = "the quick brown fox",
        verb = "jumps over"
    );

    // 在 `{}` 中指定输出格式
    println!("{0} {0:b} {0:o} {0:X}", 100);

    // 在 `{}` 中指定对齐格式
    println!("{number:>width$}", number = 1, width = 6); // 右对齐6位，前面补空格
    println!("{number:>0width$}", number = 1, width = 6); // 右对齐6位，前面补0

    let pi = 3.141592;
    println!("Pi is roughly {:.3}", pi); // 保留3位小数的形式输出pi
}
