/// 使用fmt::Display来格式化输出
use std::fmt; // 导入fmt模块使得Display可用

struct Time {
    hours: u8,
    minutes: u8,
}

// 为了使用 `{}` 标记，必须手动实现 `fmt::Display` trait
impl fmt::Display for Time {
    // 该trait要求 `fmt` 使用与下面函数完全一致的的函数签名
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // 将self的元素写入到给定的输出流 `f`
        write!(f, "{}:{}", self.hours, self.minutes)
    }
}

#[derive(Debug)]
struct Complex {
    real: f32,
    imag: f32,
}

impl fmt::Display for Complex {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // 句尾没有 `;`，此句返回一个fmt::Result
        write!(f, "{} + {}i", self.real, self.imag)
    }
}

#[allow(dead_code)]
pub fn test() {
    let time = Time {
        hours: 10,
        minutes: 30,
    };
    println!("{}", time);

    let x = Complex {
        real: 3.3,
        imag: 7.2,
    };
    println!("Display: {}", x);
    println!("Debug: {:?}", x);
}
