/// 除了std库中的类型，其它类型若想实现 `std::fmt` 的格式化输出，都必须实现至少一个可打印的 `traits`
/// 但是所有类型都可以 `derive`(自动推导) `fmt::Debug` 的实现

#[allow(dead_code)]
#[derive(Debug)] // 启用fmt::Debug自动推导实现
struct Person<'a> {
    name: &'a str,
    age: u8,
}

#[allow(dead_code)]
pub fn test() {
    println!("Running example: {}", std::module_path!().to_string());
    let name = "Rust";
    let age = 27;

    let rust = Person {
        name: name,
        age: age,
    };

    // `{:?}` 表明使用Debug进行格式输出
    println!("Debug: {:?}", rust);
    // `{:#?}` 表明使用Debug进行格式输出时进行美化
    println!("Debug with beautify: {:#?}", rust);
}
