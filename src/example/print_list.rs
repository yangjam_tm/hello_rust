use std::fmt;

struct List(Vec<i32>);

impl fmt::Display for List {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // 使用元祖下标获取值，并创建一个引用
        let vec = &self.0;

        // ？表示异常处理，若这里出错，将返回相应的错误
        write!(f, "[")?;

        // 对 vec 进行迭代
        for (count, v) in vec.iter().enumerate() {
            if count != 0 {
                write!(f, ", ")?;
            }
            write!(f, "{}: {}", count, v)?;
        }

        write!(f, "]")
    }
}

#[allow(dead_code)]
pub fn test() {
    let v = List(vec![1, 2, 3]);
    println!("{}", v);
}
