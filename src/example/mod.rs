/// example文件夹为一个模块,该模块用于进行rust语法测试
/// 代码来源于[通过例子学习Rust](https://rustwiki.org/zh-CN/rust-by-example/index.html)
/// 通过文件夹将作用相似的代码组织为一个mod(module),文件夹下的mod.rs为该模块的入口，用来管理目录下的其它文件
#[allow(dead_code)]

// 导入该模块下的子mod
mod fmt_output;
mod fmt_debug;
mod fmt_display;
mod print_list;
mod fmt_test;

// mod内的函数默认为private,需要通过 `pub` 关键字将函数暴露给外部
pub fn run_test() {
    // fmt_output::test();
    // fmt_debug::test();
    // fmt_display::test();
    // print_list::test();
    fmt_test::test();
}
