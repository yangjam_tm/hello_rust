---
author: yangjian
create: 2024-2-2
---

## 简介

- 该工程用于学习 rust 语言，对语法、常用技巧和通用库进行测试
- src: 存放测试源码